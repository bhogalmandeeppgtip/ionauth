<?php

//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(E_ALL ^ E_NOTICE);

class Excels extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->library(array('ion_auth','form_validation'));
        //$this->load->helper(array('url','language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
        $this->load->model('admin/dbtasks_model');
     }

    public function index() {


        // validate form input
        $this->form_validation->set_rules('excel_name', $this->lang->line('excel_validation_name_label'), 'required');
        $this->form_validation->set_rules('school_name', $this->lang->line('school_name_validation_label'), 'required|alpha_dash');

        if ($this->form_validation->run() == TRUE)
        {
            $new_group_id = $this->ion_auth->create_group($this->input->post('excel_name'), $this->input->post('school_name'));
            if($new_group_id)
            {
                // check to see if we are creating the group
                // redirect them back to the admin page
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("auth", 'refresh');
            }
        }
        else
        {
            // display the create group form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['excel_name'] = array(
                'class' => 'form-control',
                'name'  => 'excel_name',
                'id'    => 'excel_name',
                'type'  => 'file',
                'value' => $this->form_validation->set_value('excel_name'),
            );
            $this->data['school_name'] = array(
                'class' => 'form-control',
                'name'  => 'school_name',
                'id'    => 'school_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('school_name'),
            );

            $this->_render_page('auth/add_excels', $this->data);
        }



        // display the select excel to import form
            // set the flash data error message if there is one
           /* $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['excel_name'] = array(
                'class' => 'form-control',
                'name'  => 'excel_name',
                'id'    => 'excel_name',
                'type'  => 'file',
                'value' => $this->form_validation->set_value('excel_name'),
            );
            $this->data['school_name'] = array(
                'class' => 'form-control',
                'name'  => 'school_name',
                'id'    => 'school_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('school_name'),
            );

        $this->_render_page('auth/add_excels', $this->data);*/

    }

    public function importDataFromExcel(){
        $this->load->library('upload');
        $this->load->library('image_lib');
        $school_name = $this->input->post('school_name');
        $path = $this->config->item('excel_file_path');
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|xlsx';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = TRUE;
        $this->upload->initialize($config);
        if($this->upload->do_upload('excel_name')){
            //Read excel if uploaded successfully
            $data = $this->upload->data();
            $uploaded_excel_file_name = $data['file_name'];
            $file = './assets/uploads/excels/'.$uploaded_excel_file_name;
            //load the excel library
            $this->load->library('excel');
            //read file from path
            $objPHPExcel = PHPExcel_IOFactory::load($file);
            //get only the Cell Collection
            $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
            $rowCounter = 1; 
            $rowData = array();
            //extract to a PHP readable array format
            foreach ($cell_collection as $cell) {
                $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                //header will/should be in row 1 only. of course this can be modified to suit your need.
                if ($row == 1) {
                    $header[$row][$column] = $data_value;
                } else {
                    
                        $rowCounter = $rowCounter + 1;
                        if($column == "A"){
                            $rowData[$row] = array('school_udise_code' => ($data_value == "" ? $rowData[$row-1]['school_udise_code'] : $data_value));
                        }else if($column == "B"){
                            $rowData[$row] = array_merge($rowData[$row], array('aadhaar_no' => ($data_value == "" ? 0 : $data_value)));
                        }else if($column == "C"){
                              $rowData[$row] = array_merge($rowData[$row], array('name' => $data_value));
                        }else if($column == "D"){
                            $rowData[$row] = array_merge($rowData[$row], array('father_name' => $data_value));
                        }else if($column == "E"){
                            $rowData[$row] = array_merge($rowData[$row], array('mother_name' => ($data_value == "" ? "" : $data_value)));
                        }else if($column == "F"){
                            $rowData[$row] = array_merge($rowData[$row], array('dob' => ($data_value == "" ? date("Y-m-d") : $data_value)));
                        }else if($column == "G"){
                            $rowData[$row] = array_merge($rowData[$row], array('gender' => $data_value));
                        }else if($column == "I"){
                            $rowData[$row] = array_merge($rowData[$row], array('category' => $data_value));
                        }else if($column == "K"){
                            $rowData[$row] = array_merge($rowData[$row], array('religion' => $data_value));
                        }else if($column == "M"){
                            $rowData[$row] = array_merge($rowData[$row], array('mother_tongue' => $data_value));
                        }else if($column == "O"){
                            $rowData[$row] = array_merge($rowData[$row], array('locality' => $data_value));
                        }else if($column == "P"){
                            $rowData[$row] = array_merge($rowData[$row], array('doa' => ($data_value == "" ? date("Y-04-01") : $data_value)));
                        }else if($column == "Q"){
                            $rowData[$row] = array_merge($rowData[$row], array('admission_no' => $data_value));
                        }else if($column == "R"){
                            $rowData[$row] = array_merge($rowData[$row], array('bpl' => $data_value));
                        }else if($column == "T"){
                            $rowData[$row] = array_merge($rowData[$row], array('disadvantage_group' => $data_value));
                        }else if($column == "U"){
                            $rowData[$row] = array_merge($rowData[$row], array('free_education' => $data_value));
                        }else if($column == "X"){
                            $rowData[$row] = array_merge($rowData[$row], array('class' => $data_value));
                        }else if($column == "Z"){
                            $rowData[$row] = array_merge($rowData[$row], array('section' => $data_value));
                        }else if($column == "AB"){
                            $rowData[$row] = array_merge($rowData[$row], array('previous_class' => $data_value));
                        }else if($column == "AD"){
                            $rowData[$row] = array_merge($rowData[$row], array('class1_previous_year' => $data_value));
                        }else if($column == "AF"){
                            $rowData[$row] = array_merge($rowData[$row], array('previous_class_days' => ($data_value == "" ? 0 : $data_value)));
                        }else if($column == "AG"){
                            $rowData[$row] = array_merge($rowData[$row], array('instruction_medium' => ($data_value == "" ? "4-Hindi" : $data_value)));
                        
                        }else if($column == "AI"){
                            $rowData[$row] = array_merge($rowData[$row], array('disability' => $data_value));
                        }else if($column == "AK"){
                            $rowData[$row] = array_merge($rowData[$row], array('cswnid_facility' => $data_value));
                        }else if($column == "AM"){
                            $rowData[$row] = array_merge($rowData[$row], array('uniform_sets_previous_year' => $data_value));
                        }else if($column == "AO"){
                            $rowData[$row] = array_merge($rowData[$row], array('free_text_book_set' => $data_value));
                        }else if($column == "AQ"){
                            $rowData[$row] = array_merge($rowData[$row], array('free_transport' => $data_value));
                        }else if($column == "AS"){
                            $rowData[$row] = array_merge($rowData[$row], array('free_escort' => $data_value));
                        }else if($column == "AU"){
                            $rowData[$row] = array_merge($rowData[$row], array('mdm' => $data_value));
                        }else if($column == "AW"){
                            $rowData[$row] = array_merge($rowData[$row], array('free_hostel' => $data_value));
                        }else if($column == "AY"){
                            $rowData[$row] = array_merge($rowData[$row], array('special_training' => $data_value));
                        }else if($column == "BA"){
                            $rowData[$row] = array_merge($rowData[$row], array('homeless' => $data_value));
                        }else if($column == "BC"){
                            $rowData[$row] = array_merge($rowData[$row], array('appeared_last_exam' => $data_value));
                        }else if($column == "BE"){
                            $rowData[$row] = array_merge($rowData[$row], array('result_last_exam' => $data_value));
                        }else if($column == "BG"){
                            $rowData[$row] = array_merge($rowData[$row], array('percentage_last_exam' => ($data_value == "" ? "0" : $data_value)));
                        }else if($column == "BH"){
                            $rowData[$row] = array_merge($rowData[$row], array('stream' => $data_value));
                        }else if($column == "BJ"){
                            $rowData[$row] = array_merge($rowData[$row], array('trade_sector' => $data_value));
                        }else if($column == "BL"){
                            $rowData[$row] = array_merge($rowData[$row], array('ifc' => $data_value));
                        }else if($column == "BN"){
                            $rowData[$row] = array_merge($rowData[$row], array('deworming_tab' => $data_value));
                        }else if($column == "BP"){
                            $rowData[$row] = array_merge($rowData[$row], array('vitamim_a_tab' => $data_value));
                        }else if($column == "BR"){
                            $rowData[$row] = array_merge($rowData[$row], array('bank_account' => ($data_value == "" ? "0" : $data_value)));
                        }else if($column == "BS"){
                            $rowData[$row] = array_merge($rowData[$row], array('ifsc' => ($data_value == "" ? "0" : $data_value)));
                        }else if($column == "BT"){
                            $rowData[$row] = array_merge($rowData[$row], array('mobile' => ($data_value == "" ? "0" : $data_value)));
                        }else if($column == "BU"){
                            $rowData[$row] = array_merge($rowData[$row], array('email' => ($data_value == "" ? "0" : $data_value)));
                            $rowData[$row] = array_merge($rowData[$row], array('school_name' => $school_name));
                            if($rowData[$row]['aadhaar_no'] == "" && $rowData[$row]['name'] == "" && $rowData[$row]['father_name'] == ""  && $rowData[$row]['mother_name'] == "" ){
                                //
                            }else{
                                
                                $compare_exist_array = array('school_udise_code' => $rowData[$row]['school_udise_code'], 'aadhaar_no' => $rowData[$row]['aadhaar_no'], 'name' => $rowData[$row]['name'], 'mother_name' => $rowData[$row]['mother_name']);
                                $compare_result = $this->dbtasks_model->fetchRowFields("excel_random_data","sno",$compare_exist_array);
                                if($compare_result){
                                    //echo "Record Already Exist"; 
                                }else{
                                    $this->db->insert('excel_random_data', $rowData[$row]);
                                }

                            }
                        }else{
                            //echo "default";
                        }    
                    // echo "<br />inside for loop<br />row=".$row;echo "<br />column= ".$column;echo "<br />data_value= ".$data_value;
                }
            }

            // echo "<br /><br /><br /><br /><br /><br /><br />rowData values in array"; print_r($rowData);
            //send the data in an array format
            // $data['header'] = $header;
            // print_r($data['header']);
            // $data['values'] = $arr_data;
            // print_r($data['values']);


            //excel reader library end

        }else{
            echo "Problem In Uploading File Please Try Later";
            exit;
        }
        
        


            
    }


    // duplicate in all controllers need to modify
    function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }
}
