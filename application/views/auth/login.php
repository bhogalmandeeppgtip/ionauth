<?php include('common/header.php'); ?>

<div class="container">
    <div class="row">
        <div id="infoMessage"><?php echo $message;?></div>
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class=""><?php echo lang('login_heading');?></strong>
                </div>
                <div class="panel-body">
                    <?php echo form_open('auth/login',array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'post'));?>
                        <div class="form-group">
                            <?php echo lang('login_identity_label', 'inputEmail3','class="col-sm-3 control-label"');?>
                            <div class="col-sm-9">
                                <?php echo form_input($identity);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo lang('login_password_label', 'password','class="col-sm-3 control-label"');?></label>
                            <div class="col-sm-9">
                                <?php echo form_input($password);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <div class="checkbox">
                                    <label class="">
                                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"' );?>
                                        <?php echo lang('login_remember_label', 'remember');?>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <?php echo form_submit('submit', lang('login_submit_btn'),'class="btn btn-success btn-sm"');?>
                                <?php echo form_button('reset', lang('login_reset_btn'),'class="btn btn-default btn-sm"');?>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                  <?php echo anchor('auth/create_user', 'Create account')?> | <?php echo anchor('auth/forgot_password', 'Need help ?')?> 
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('common/footer.php'); ?>

