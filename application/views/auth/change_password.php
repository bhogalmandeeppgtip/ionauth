<?php include('common/header.php'); ?>

<h1><?php echo lang('change_password_heading');?></h1>
<p><?php echo lang('change_password_subheading');?></p>
<div class="panel panel-default">
      <div class="panel-heading">Panel heading</div>
</div>
      <div class="container">
            <div id="infoMessage"><?php echo $message;?></div>

            <?php echo form_open("auth/change_password");?>

                  <p>
                        <?php echo lang('change_password_old_password_label', 'old_password');?> <br />
                        <?php echo form_input($old_password);?>
                  </p>

                  <p>
                        <label for="new_password"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?></label> <br />
                        <?php echo form_input($new_password);?>
                  </p>

                  <p>
                        <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?> <br />
                        <?php echo form_input($new_password_confirm);?>
                  </p>

                  <?php echo form_input($user_id);?>
                  <p><?php echo form_submit('submit', lang('change_password_submit_btn'));?></p>

            <?php echo form_close();?>
      </div>      

      <p><?php echo anchor('', lang('index_heading'))?> | <?php echo anchor('auth/change_password', lang('index_change_password_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?> | <?php echo anchor('auth/logout', 'Logout')?> </p>

<?php include('common/footer.php'); ?>