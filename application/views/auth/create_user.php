<?php include('common/header.php'); ?>

    <div class="container">
      <div id="infoMessage"><?php echo $message;?></div>
      <div class="row">
        <div class="col-md-4 col-md-offset-7">
          <div class="panel panel-default">
            <div class="panel-heading"> <strong class=""><?php echo lang('create_user_heading');?></strong> </div>
            <div class="panel-body">
              <?php echo form_open('auth/create_user',array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'post'));?>
                <div class="form-group">
                    <?php echo lang('create_user_fname_label', 'first_name','class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($first_name);?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_lname_label', 'inputEmail3', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($last_name);?>
                    </div>
                </div>
               <?php
                if($identity_column!=='email') {
                    echo '<p>';
                    echo lang('create_user_identity_label', 'identity');
                    echo '<br />';
                    echo form_error('identity');
                    echo form_input($identity);
                    echo '</p>';
                }
                ?>
                <div class="form-group">
                    <?php echo lang('create_user_company_label', 'company', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($company);?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_email_label', 'email', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($email);?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_phone_label', 'phone', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($phone);?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_password_label', 'password', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($password);?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo lang('create_user_password_confirm_label', 'password_confirm', 'class="col-sm-3 control-label"');?>
                    <div class="col-sm-9">
                      <?php echo form_input($password_confirm);?>
                    </div>
                </div>
                <div class="form-group last">
                  <div class="col-sm-offset-3 col-sm-9">
                    <?php echo form_submit('submit', lang('create_user_submit_btn'),'class="btn btn-success btn-sm"');?>
                    <?php echo form_button('reset', lang('login_reset_btn'),'class="btn btn-default btn-sm"');?>
                  </div>
                </div>
              <?php echo form_close();?>
            </div>
            <div class="panel-footer">
              <?php echo anchor('auth/login', 'Login')?> | <?php echo anchor('auth/forgot_password', 'Need help ?')?> 
            </div>
          </div>    
        </div>
      </div>
    </div>

<?php include('common/footer.php'); ?>
