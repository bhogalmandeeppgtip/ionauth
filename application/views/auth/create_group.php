<?php include('common/header.php'); ?>

    <h1><?php echo lang('create_group_heading');?></h1>
	<p><?php echo lang('create_group_subheading');?></p>
    <div class="panel panel-default">
      <div class="panel-heading">Panel heading</div>
    </div>
    <div class="container">
		<div id="infoMessage"><?php echo $message;?></div>
			<?php echo form_open("auth/create_group");?>

			      <p>
			            <?php echo lang('create_group_name_label', 'group_name');?> <br />
			            <?php echo form_input($group_name);?>
			      </p>

			      <p>
			            <?php echo lang('create_group_desc_label', 'description');?> <br />
			            <?php echo form_input($description);?>
			      </p>

			      <p><?php echo form_submit('submit', lang('create_group_submit_btn'));?></p>

			<?php echo form_close();?>
	 </div>
    </div>
      <p><?php echo anchor('', lang('index_heading'))?> | <?php echo anchor('auth/change_password', lang('index_change_password_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?> | <?php echo anchor('auth/logout', 'Logout')?> </p>

<?php include('common/footer.php'); ?>
