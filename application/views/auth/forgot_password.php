<?php include('common/header.php'); ?>

<div class="container">
    <div class="row">
        <div id="infoMessage"><?php echo $message;?></div>
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class=""><?php echo lang('forgot_password_heading');?></strong></div>
                <div class="panel-body">
                    <?php echo form_open("auth/forgot_password",array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'post'));?>
                        <div class="form-group">
                            <label for="identity" class="col-sm-3 control-label"><?php echo (($type=='email') ? sprintf(lang('forgot_password_email_label'), $identity_label) : sprintf(lang('forgot_password_identity_label'), $identity_label));?></label>
                            <div class="col-sm-9">
                                <?php echo form_input($identity);?>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <?php echo form_submit('submit', lang('forgot_password_submit_btn'), 'class="btn btn-success btn-sm"'); ?>
                            </div>
                        </div>

                    <?php echo form_close();?>
                </div>
                <div class="panel-footer">
                  <?php echo anchor('auth/login', 'Login')?> | <?php echo anchor('auth/create_user', 'Create User')?> 
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('common/footer.php'); ?>










<!-- 
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Registration Template for Bootstrap</title>
    <link href="../../assets/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="../../assets/html/css/custom.css" rel="stylesheet">
  </head>
  <body>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-7">
            <div class="panel panel-default">
                <div class="panel-heading"> <strong class=""><?php //echo lang('forgot_password_heading');?></strong>

                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="post" action="forgot_password">
                        
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input name="identity" type="text" class="form-control" id="inputEmail3" placeholder="email" required="">
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-offset-3 col-sm-9">
                                <button type="submit" class="btn btn-success btn-sm"><?php //echo lang('forgot_password_submit_btn'); ?></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                  <?php //echo anchor('auth/login', 'Login')?> | <?php //echo anchor('auth/create_user', 'Create User')?> 
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html> -->
