<?php include('common/header.php'); ?>

    <h1><?php echo lang('deactivate_heading');?></h1>
    <div class="panel panel-default">
      <div class="panel-heading">Panel heading</div>
    </div>
      <div class="container">
		<p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>
		<?php echo form_open("auth/deactivate/".$user->id);?>
		  <p>
		  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
		    <input type="radio" name="confirm" value="yes" checked="checked" />
		    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
		    <input type="radio" name="confirm" value="no" />
		  </p>

		  <?php echo form_hidden($csrf); ?>
		  <?php echo form_hidden(array('id'=>$user->id)); ?>

		  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>

		<?php echo form_close();?>
	 </div>
    </div>
      <p><?php echo anchor('', lang('index_heading'))?> | <?php echo anchor('auth/change_password', lang('index_change_password_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?> | <?php echo anchor('auth/logout', 'Logout')?> </p>

<?php include('common/footer.php'); ?>
