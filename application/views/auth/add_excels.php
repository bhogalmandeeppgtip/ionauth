<?php include('common/header.php'); ?>

    <h1><?php echo 'Import Data';?></h1>
	<p><?php echo 'Select Excel File To Import Data';?></p>
    <div class="panel panel-default">
      <div class="panel-heading">Panel heading</div>
    </div>
    <div class="container">
		<div id="infoMessage"><?php echo $message;?></div>
			<?php echo form_open_multipart("excels/importDataFromExcel");?>

			      <p>
			            <?php echo lang('select_excel_label', 'excel_name');?> <br />
			            <?php echo form_input($excel_name);?>
			      </p>

			      <p>
			            <?php echo lang('excel_school_name_label', 'school_name');?> <br />
			            <?php echo form_input($school_name);?>
			      </p>

			      <p><?php echo form_submit('submit', lang('upload_excel_submit_btn'));?></p>

			<?php echo form_close();?>
	 </div>
    </div>
      <p><?php echo anchor('', lang('index_heading'))?> | <?php echo anchor('auth/change_password', lang('index_change_password_link'))?> | <?php echo anchor('auth/create_group', lang('index_create_group_link'))?> | <?php echo anchor('auth/logout', 'Logout')?> </p>

<?php include('common/footer.php'); ?>
