<?php
/* * ***************************************************** */
# Page : Database Operation Page 
# Desc : Add/Update/remove/change status & level actions
# Author : Mandeep Kumar Bhogal
/* * ***************************************************** */

class Dbtasks_model extends CI_Model {

    //public $table = '';

    //put your code here
    public function __construct() {
        parent::__construct();
        //$this->table = "users";
    }

    # TO get limited number of rows.
    # @param1 => Count starts from
    # @param2 => Number of Counts
    # @param3 => Where Condition
    # @param4 => Order By	

    public function getLimit($table, $start, $limit, $fields = NULL, $where = NULL, $order = NULL) {
        $this->db->select($fields);
        $this->db->limit($limit, $start);
        if($where){ $this->db->where($where); } 
        $this->db->order_by($order); 
        $query = $this->db->get($table);
        return $query->result();
    }
    // custom +
    # TO get limited number of rows.
    # @param1 => Count starts from
    # @param2 => Number of Counts
    # @param3 => Where Condition
    # @param4 => Order By   

    public function getLimitWithFunction($table, $start, $limit, $fields = NULL, $where = NULL, $whereWithFunction = NULL, $order = NULL) {
        $this->db->select($fields);
        $this->db->limit($limit, $start);
        if($where){ $this->db->where($where); } 
        if($whereWithFunction){
            $run = "FIND_IN_SET('".$whereWithFunction."', subjects)";
            $this->db->where($run);
        } 
        $this->db->order_by($order); 
        $query = $this->db->get($table);
        return $query->result();
    }

    # To get all data from a single row 
    # @param1 => Fetch the full row as per condition on @param2
    # @param2 => Where Condition
    # @param3 => Order of the Query

    public function fetchRowFields($table, $fields, $where = NULL, $order = NULL) {
        if($fields){   $this->db->select($fields);  }
        if($where){ $this->db->where($where); } 
        if($order){ $this->db->order_by($order); }
        $query = $this->db->get($table);
        return $query->result_array();
    }
   # TO get limited number of fields from multiple rows.
	# @param1 => Fields which we required on result
	# @param2 => Where Condition
    public function getFields($table, $fields, $where = NULL, $order = NULL,$group = NULL)
    {
        if($fields){   $this->db->select($fields);  }
        if($where){ $this->db->where($where); } 
        if($order){ $this->db->order_by($order); }
	$query = $this->db->get($table);
        return $query->result_array();
    }
    
    
    # Update data into database
    # @param1 => Array to be updated
    # @param2 => Condition  

    public function update($table, $updateArray, $conditionArray) {
        $this->db->trans_start();
        // print_r($value);echo "<br>";print_r($condition);exit;
        $this->db->where($conditionArray);
        $this->db->update($table, $updateArray);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            return false;
        } else {
            return true;
        }
    }

    # Insert data into database
    # @param1 => Array to be inserted 

    public function insert($table, $InsertArray) {

        $this->db->insert($table, $InsertArray);
        return $this->db->insert_id();
    }

    # Count the with respect to the condition
    # @param1 => Condition

    public function limitCount($table, $sql = NULL) {
        $query = $this->db->get($table);
        return $query->num_rows();
    }
//new added function
    # Count the with respect to the condition
    # @param1 => Condition

    public function limitCountWithCondition($table, $where = NULL) {
        if($where){ $this->db->where($where); } 
        $query = $this->db->get($table);
        return $query->num_rows();
    }
    //
    # Delete data from database
    # @param1 => Condition if Exists  

    public function delete($table, $field, $id) {
        //return $this->db->delete($table, array('id' => $id));
        return $this->db->delete($table, array($field => $id));
    }

    # TO get single field from multiple rows.
    # @param1 => Field which we required on result
    # @param2 => Where Condition

    public function getSingleField($table, $field, $where = NULL) {
        $field = ($field) ? " $field " : " * ";
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }
    
    # TO Check the the data already exists or not
    # @param1 => Where condition
    function isExist($table, $where = NULL)	{
          /*  $where		= ($where) ? " $where " : "";
            $res  		= $this->count($where);
            if ($res >= 1)
                    return true;
            return false;  */
    }
    
    #custom Query subject
    function customQuerysubject($stan, $sub){
        $query = $this->db->query("
SELECT id,name,rollno,subjects,standard,stream,FIND_IN_SET('$sub', students.subjects) AS temp FROM students where standard=$stan");
        // $query = $this->db->query($query);
        return $query->result_array();
    }  
    #custom Query standard
    function customQuerystandard($stan){
        $query = $this->db->query("
            SELECT id,name,rollno,subjects,standard,stream,'1' as 'temp' FROM students where standard=$stan");
        // $query = $this->db->query($query);
        return $query->result_array();
    }    
    #custom Query standard Attendance
    function customQuerystandardAttendance($date, $stan){
        // $today = date('Y-m-d');
        $today = $date;
        $query = $this->db->query("
            SELECT at.id,at.date,at.standard,at.stream,at.subject,at.studentId,at.value,at.time,st.name as stname,st.rollno,sb.name as sbname FROM attendances as at 
            LEFT JOIN students as st ON st.id = at.studentId
            LEFT JOIN subjects as sb ON sb.id = at.subject
            where at.standard = $stan AND at.date = '$today'");
        return $query->result_array();
    }
    #custom Query subject Attendance
    function customQuerysubjectAttendance($date, $stan, $sub){
        // $today = date('Y-m-d');
        $today = $date;
        $query = $this->db->query("
            SELECT at.id,at.date,at.standard,at.stream,at.subject,at.studentId,at.value,at.time,st.name as stname,st.rollno,sb.name as sbname FROM attendances as at 
            LEFT JOIN students as st ON st.id = at.studentId
            LEFT JOIN subjects as sb ON sb.id = at.subject
            where at.standard = $stan AND at.subject = $sub AND at.date = '$today'");
        return $query->result_array();
    }  
    # Download table from database 

    public function download_table($table) {
        $query = $this->db->get($table);
        return $query->result_array();
    }
    
    
    #Get Table fields as an array
    function tablesFields($table){
		$result = mysql_query("SHOW COLUMNS FROM ".$table);
		if (!$result)
		{
			//$this->getError(mysql_error());
		}
		if (mysql_num_rows($result) > 0)
		{
			$structure = array();
			//while ($row = mysql_fetch_assoc($result))
                        while ($row = mysql_fetch_array($result))    
			{
				//$structure[$row['Field']] = '';
                                $structure[] = $row['Field'];
			}
		}
		return $structure;
	}
      
         #Get Tables from database
         function dbTables(){
		$result = mysql_query("SHOW tables");  
               if (!$result)
		{
			//$this->getError(mysql_error());
		}
		if (mysql_num_rows($result) > 0)
		{
			$structure = array();
                        while ($row = mysql_fetch_array($result))    
			{
				//$structure[$row['Field']] = '';
                                $structure[] = $row['Tables_in_ciadmin'];
			}
		}
                //  print_r($structure);exit;
		return $structure;
	}

}

?>
